package com.service.javabitbucket.controller;


import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.apache.servicecomb.provider.rest.common.RestSchema;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.CseSpringDemoCodegen", date = "2018-05-17T19:22:09.856Z")

@RestSchema(schemaId = "javabitbucket")
@RequestMapping(path = "/javabitbucket", produces = MediaType.APPLICATION_JSON)
public class JavabitbucketImpl {

    @Autowired
    private JavabitbucketDelegate userJavabitbucketDelegate;


    @RequestMapping(value = "/helloworld",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    public String helloworld( @RequestParam(value = "name", required = true) String name){

        return userJavabitbucketDelegate.helloworld(name);
    }

}
